<?php

class manage_git {
  function __construct($domain, $username, $prefix, $ssh_key, $repo_path) {
    $this->domain = $domain;
    $this->username = $username;
    $this->prefix = $prefix;
    $this->ssh_key = $ssh_key;
    $this->repo_path = $repo_path;
  }

  function exec($command, $where = './') {
    $creds = $this->username.'@'.$this->domain;
    $cmd = array();
    $cmd[] = 'cd '.$where;
    $cmd[] = $command;
    $cmd = implode($cmd, ';');
    $cmd .= ' 2>&1';

    $cmd = 'ssh -o StrictHostKeyChecking=no -i ' . $this->ssh_key . ' ' . $creds . " '" . $cmd . "'";
    
    $output = shell_exec($cmd);
    return $output;
  }

  function status() {
    return $this->exec('git status', $this->repo_path);
  }

  function fetch() {
    return $this->exec('git fetch', $this->repo_path);
  }

  function pull() {
    return $this->exec('git pull', $this->repo_path);
  }

  function repo_path() {
    return $this->repo_path;
  }

  function uncommited() {
    $git_files = $this->exec('git status -s', $this->repo_path);
    $git_files = explode("\n", $git_files);

    $files = array();
    foreach ($git_files as $file) {
      if(!empty($file))
        $files[substr($file, 3)] = $file;
    }

    return $files;
  }

  function references($prefix = '') {
    $references['Branches'] = $this->branches($prefix);
    $references['Tags'] = $this->tags($prefix);

    return $references;
  }

  function branches($prefix = '') {
    if(empty($prefix))
      $git_branches = $this->exec('git branch', $this->repo_path);
    else {
      $git_branches  = $this->exec('git branch | grep "^'.$prefix.'"', $this->repo_path);
      $git_branches .= $this->exec('git branch | grep "^\*"', $this->repo_path);
    }
    $git_branches = explode("\n", $git_branches);

    $branches = array();
    foreach ($git_branches as $branch) {
      if(!empty($branch)) {
        $status = substr($branch, 0, 2);
        $branch = substr($branch, 2);

        if ($status == '* ')
          $branches['*'] = $branch;
        else
          $branches[$branch] = $branch;
      }
    }

    return $branches;
  }

  function tags($prefix = '') {
    if(empty($prefix))
      $git_tags = $this->exec('git tag', $this->repo_path);
    else
      $git_tags = $this->exec('git tag | grep "^'.$prefix.'"', $this->repo_path);
    $git_tags = explode("\n", $git_tags);

    $tags = array();
    foreach ($git_tags as $tag) {
      if(!empty($tag)) {
        $tags[$tag] = $tag;
      }
    }

    return $tags;
  }

  function checkout($ref) {
    $git_checkout = $this->exec('git checkout ' . $ref, $this->repo_path);
    $lines = explode("\n", $git_checkout);

    return $lines[0];
  }

  function branch_info() {
    $reference_info = trim($this->exec('git branch | grep "*"', $this->repo_path));

    return $reference_info;
  }

  function tag_info() {
    $commit_info = $this->commit_info();
    $refs = explode(', ', $commit_info);
    $prefix = '/^'.$this->prefix.'/';
    $refs = preg_grep($prefix, $refs);

    return $refs;
  }

  function commit_info() {
    $commit_info = trim($this->exec("git log --pretty=format:'%d' -1", $this->repo_path));
    $commit_info = str_replace(array('(',')'), '', $commit_info);
    return $commit_info;
  }

  function add($files) {
    $files = implode(' ', $files);
    $add_info = trim($this->exec('git add ' . $files, $this->repo_path));

    return $add_info;
  }

  function commit($message) {
    $commit_info = trim($this->exec('git commit -m "' . $message . '"', $this->repo_path));

    return $commit_info;
  }
}