<?php

function environments_manage() {
  $environments = entity_load('environment');

  $header = array(t('Name'), t('Description'), t('Prefix'), array('data' => t('Operations'), 'colspan' => '4'));
  $rows = array();
  foreach ($environments as $environment) {
    $rows[] = array(
      $environment->name,
      $environment->description,
      $environment->prefix,
      l(t('Edit'), 'admin/project/manage-environments/' . $environment->name . '/edit'),
      l(t('Delete'), 'admin/project/manage-environments/' . $environment->name . '/delete'),
    );
  }

  $output  = theme('table', array('header' => $header, 'rows' => $rows));
  
  return $output;
}

function environments_add_form() {
  $form['name'] = array(
    '#type' => 'textfield', 
    '#title' => t('Name'), 
    '#default_value' => '',
    '#required' => TRUE,
    '#weight' => 100
  );

  $form['prefix'] = array(
    '#type' => 'textfield', 
    '#title' => t('Prefix'), 
    '#default_value' => '', 
    '#required' => FALSE,
    '#weight' => 200

  );

  $form['domain'] = array(
    '#type' => 'textfield', 
    '#title' => t('Domain or IP'), 
    '#default_value' => '', 
    '#required' => FALSE,
    '#weight' => 300

  );

  $form['username'] = array(
    '#type' => 'textfield', 
    '#title' => t('Username'), 
    '#default_value' => '', 
    '#required' => FALSE,
    '#weight' => 400

  );

  $form['path'] = array(
    '#type' => 'textfield', 
    '#title' => t('Path'), 
    '#default_value' => '', 
    '#required' => FALSE,
    '#weight' => 500

  );

  $form['keypath'] = array(
    '#type' => 'textfield', 
    '#title' => t('Key Path'), 
    '#default_value' => '', 
    '#required' => FALSE,
    '#weight' => 600

  );

  $form['description'] = array(
    '#type' => 'textarea', 
    '#title' => t('Description'), 
    '#default_value' => '',
    '#weight' => 700

  );

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Add Environment'),
    '#weight' => 800

  );

  return $form;
}

function environments_add_form_validate($form, &$form_state) {
  $name = $form_state['values']['name'];

  // Content name must be alphanumeric or underscores.
  if (preg_match('/[^a-zA-Z0-9_]/', $name))
    form_error($form['name'], t('The name must only use alphanumeric or underscores.'));

  $env = environment_load($name);

  if ($env)
    form_error($form['name'], t('The name must be unique.'));
}

function environments_add_form_submit($form, &$form_state) {
  $env              = environment_from_form_state($form_state);
  $env->name        = $form_state['values']['name'];

  entity_save('environment', $env);
  drupal_set_message('Environment created.');
  $form_state['redirect'] = 'admin/project/manage-environments';
}

function environments_edit_form($form, &$form_state, $env) {
  $form_state['env'] = $env;
  $form = environments_add_form();
  $form['name']['#disabled'] = TRUE;

  $form['name']['#default_value']        = $env->name;
  $form['prefix']['#default_value']      = $env->prefix;
  $form['description']['#default_value'] = $env->description;
  $form['domain']['#default_value']      = $env->domain;
  $form['username']['#default_value']    = $env->username;
  $form['path']['#default_value']        = $env->path;
  $form['keypath']['#default_value']        = $env->keypath;

  $form['submit']['#value'] = 'Save environment';
  return $form;
}

function environments_edit_form_validate($form, &$form_state) {}

function environments_edit_form_submit($form, &$form_state) {
  $env = environment_from_form_state($form_state, $form_state['env']);

  entity_save('environment', $env);
  drupal_set_message('Environment saved.');
  $form_state['redirect'] = 'admin/project/manage-environments';
}

function environment_from_form_state($form_state, &$env = FALSE) {
  if(!$env)
    $env = (object) NULL;

  $env->prefix      = $form_state['values']['prefix'];
  $env->description = $form_state['values']['description'];
  $env->path        = $form_state['values']['path'];
  $env->domain      = $form_state['values']['domain'];
  $env->username    = $form_state['values']['username'];
  $env->keypath     = $form_state['values']['keypath'];

  if (!isset($env->data) && !is_array($env->data)) {
    $env->data = array();
  }

  foreach ($form_state['values'] as $key => $val) {
    if ( $key != 'name' 
      || $key != 'prefix' 
      || $key != 'domain' 
      || $key != 'username' 
      || $key != 'path' 
      || $key != 'keypath' 
      || $key != 'description' 
      || $key != 'submit' 
      || $key != 'form_build_id' 
      || $key != 'form_token' 
      || $key != 'form_id' 
      || $key != 'form_op' 
      || $key != 'description') {
      $env->data[$key] = $val;
    }
  }

  return $env;
}

function environments_delete_confirm($form, &$form_state, $env) {
  $form_state['env'] = $env;
  $form = array();

  $cancel = 'admin/project/manage-environments';
  if (!empty($_REQUEST['cancel']))
    $cancel = $_REQUEST['cancel'];

  return confirm_form($form,
    t('Are you sure you want to delete the environment %name?', array('%name' => $env->name)),
    $cancel,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

function environments_delete_confirm_submit($form, &$form_state) {
  $form_state['env']->delete();
  drupal_set_message(t('The environment has been deleted.'));
  $form_state['redirect'] = 'admin/project/manage-environments';
}
