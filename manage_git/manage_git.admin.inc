<?php

function manage_git_environments() {
  $environments = entity_load('environment');

  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => '3'));
  $rows = array();
  foreach ($environments as $environment) {
    $rows[] = array(
      $environment->name." ({$environment->prefix})",
      l('Checkout', 'admin/project/manage-code/git/'.$environment->name.'/checkout'),
      l('Status', 'admin/project/manage-code/git/'.$environment->name.'/status'),
      l('Commit', 'admin/project/manage-code/git/'.$environment->name.'/commit')
    );
  }

  $output  = theme('table', array('header' => $header, 'rows' => $rows));

  return $output;
}

function manage_git_status($env) {
  $manage_git = manage_git_object_from_env($env);
  
  return '<pre>'.$manage_git->status().'</pre>';
}

function manage_git_commit_form($form, &$form_state, $env) {
  $manage_git = manage_git_object_from_env($env);
  $form_state['mg'] = $manage_git;

  $form['git-commit']['commit-files'] = array(
    '#type' => 'select',
    '#title' => t('Files'),
    '#multiple' => TRUE,
    '#default_value' => '',
    '#options' => $manage_git->uncommited(),
  );

  $form['git-commit']['commit-message'] = array(
    '#type' => 'textarea',
    '#title' => t('Commit Message'),
    '#default_value' => '',
  );

  $form['git-commit']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Commit'),
  );

  return $form;
}

function manage_git_commit_form_submit($form, &$form_state) {
  $files = $form_state['values']['commit-files'];
  $message = $form_state['values']['commit-message'];
  $manage_git = $form_state['mg'];

  $err = $manage_git->add($files);
  if (!empty($err)) {
    drupal_set_message($err, 'warning');
  }

  $err = $manage_git->commit($message);
  if (!empty($err)) {
    drupal_set_message($err, 'status');
  }
}

function manage_git_checkout_form($form, &$form_state, $env) {
  $manage_git = manage_git_object_from_env($env);
  $form_state['mg'] = $manage_git;

  $branch_info = $manage_git->branch_info();
  $commit_info = $manage_git->commit_info();

  $ref_markup = "
<label>Branch: </label><span>{$branch_info}</span>
<label>Commit: </label><span>{$commit_info}</span>
";
  $form['ref-info'] = array(
    '#markup' => $ref_markup,
  );

  $form['git-checkout']['checkout-reference'] = array(
    '#type' => 'select',
    '#title' => t('Tag/Branch'),
    '#default_value' => '*',
    '#options' => $manage_git->references(),
  );

  $form['git-checkout']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Checkout'),
  );

  if (module_exists('environments')) {
    $form['git-checkout']['checkout-reference']['#options'] = $manage_git->references($env->prefix);
    $form['git-checkout']['checkout-reference']['#description'] = "Environments module is enabled. Only showing prefixed references.";
  }

  return $form;
}

function manage_git_checkout_form_submit($form, &$form_state) {
  $manage_git = $form_state['mg'];

  $ref = $form_state['values']['checkout-reference'];

  $message = $manage_git->checkout($ref);

  drupal_set_message($message);
}

