<?php

function project_manager_config_form() {

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
    '#weight' => 300,
    '#submit' => array('project_manager_config_form_submit'),
  );

  return $form;
}

function project_manager_config_form_submit($form, &$form_state) {

  drupal_set_message('Settings saved.');
}

function project_manager_admin_menu_block_page() {
  module_load_include('inc', 'system', 'system.admin');
  return system_admin_menu_block_page();
}

